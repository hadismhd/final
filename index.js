const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const AuthRoute = require('./routes/Auth.route');
// const authRoute = require("./routes/auth");

dotenv.config();

// main().catch(err => console.log(err));
async function main() {
  await mongoose.connect(process.env.MONGO_URL);
}

app.use('/api/auth', AuthRoute);

app.listen(8800, ()=>{
    console.log('backend server is running');
})